<?php
        require '../model/pegawai.php'; 
        session_start();             
        $pegawaitb=isset($_SESSION['pegawai'])?unserialize($_SESSION['pegawaitbl0']):new pegawai();            
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="../libs/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Tambah Data Pegawai</h2>
                    </div>
                    <p>Input Data Pegawai.</p>
                    <form action="../index.php?act=add" method="post" >
                        <div class="form-group <?php echo (!empty($pegawaitb->nip_msg)) ? 'has-error' : ''; ?>">
                            <label>NIP</label>
                            <input type="text" name="nip" class="form-control" value="<?php echo $pegawaitb->nip; ?>">
                            <span class="help-block"><?php echo $pegawaitb->nip_msg;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($pegawaitb->name_msg)) ? 'has-error' : ''; ?>">
                            <label>Nama Pegawai</label>
                            <input name="nama" class="form-control" value="<?php echo $pegawaitb->nama; ?>">
                            <span class="help-block"><?php echo $pegawaitb->name_msg;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($pegawaitb->jabatan_msg)) ? 'has-error' : ''; ?>">
                            <label>Jabatan</label>
                            <input name="jabatan" class="form-control" value="<?php echo $pegawaitb->jabatan; ?>">
                            <span class="help-block"><?php echo $pegawaitb->jabatan_msg;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($pegawaitb->satker_msg)) ? 'has-error' : ''; ?>">
                            <label>Unitkerja</label>
                            <input name="satker" class="form-control" value="<?php echo $pegawaitb->unitkerja; ?>">
                            <span class="help-block"><?php echo $pegawaitb->satker_msg;?></span>
                        </div>
                        <input type="submit" name="addbtn" class="btn btn-primary" value="Submit">
                        <a href="../index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>