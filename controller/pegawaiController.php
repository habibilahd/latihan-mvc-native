<?php
    require 'model/pegawaiModel.php';
    require 'model/pegawai.php';
    require_once 'config.php';

    session_status() === PHP_SESSION_ACTIVE ? TRUE : session_start();
    
	class pegawaiController 
	{

 		function __construct() 
		{          
			$this->objconfig = new config();
			$this->objsm =  new pegawaiModel($this->objconfig);
		}
        // mvc handler request
		public function mvcHandler() 
		{
			$act = isset($_GET['act']) ? $_GET['act'] : NULL;
			switch ($act) 
			{
                case 'add' :                    
					$this->insert();
					break;						
				case 'update':
					$this->update();
					break;				
				case 'delete' :					
					$this -> delete();
					break;								
				default:
                    $this->list();
			}
		}		
        // page redirection
		public function pageRedirect($url)
		{
			header('Location:'.$url);
		}	
        
        // add new record
		public function insert()
		{
            // print_r($_POST);
            // die();
            try{
                $pegawaitb=new pegawai();
                if (isset($_POST['addbtn'])) 
                {   
                    // read form value
                    $pegawaitb->nip = trim($_POST['nip']);
                    $pegawaitb->nama = trim($_POST['nama']);
                    $pegawaitb->jabatan = trim($_POST['jabatan']);
                    $pegawaitb->unitkerja = trim($_POST['satker']);
                    //call validation
                    // print_r($pegawaitb);
                    // print_r($_POST);
                    // die();
                    $pid = $this -> objsm ->insertRecord($pegawaitb);
                    if($pid){			
                        $this->list();
                    }else{
                        echo "Somthing is wrong..., try again.";
                    }
                }
            }catch (Exception $e) 
            {
                die('gagal pisan');
                $this->close_db();	
                throw $e;
            }
        }
        // update record
        public function update()
		{
            return 'update sini';
        }
        // delete record
        public function delete()
		{
            return 'delete ini';
        }
        public function list(){
            $result=$this->objsm->selectRecord(0);
            include "view/list.php";                                        
        }
    }
		
	
?>